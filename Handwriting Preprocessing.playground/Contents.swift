import UIKit
import PlaygroundSupport

class ViewController: UIViewController {

    var input: GlyphView = {
        let view = GlyphView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        return view
    }()

    var processed: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .purple
        view.contentMode = .scaleAspectFit
        return view
    }()

    var path = UIBezierPath()

    var points: [CGPoint] = []

    var timer: Timer?

    var isCompound = true

    let lineWidth: CGFloat = 20

    let processSize = CGSize(width: 28, height: 28)

    let digitSize = CGSize(width: 20, height: 20)

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Preprocessing Handwriting"
        view.backgroundColor = .white

        view.addSubview(input)
        input.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        input.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        input.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 20).isActive = true
        input.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 2.0/3.0).isActive = true
        input.lineWidth = lineWidth

        view.addSubview(processed)
        processed.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        processed.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        processed.topAnchor.constraint(equalTo: input.bottomAnchor, constant: 20).isActive = true
        processed.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor, constant: -20).isActive = true

        let pan = UIPanGestureRecognizer(target: self, action: #selector(drawLine))
        input.addGestureRecognizer(pan)

    }

    func drawLine(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .began:
            if !isCompound {
                path = UIBezierPath()
                isCompound = true
            }
            path.move(to: sender.location(in: input))
            input.path = path
            timer?.invalidate()
        case .changed:
            path.addLine(to: sender.location(in: input))
            input.path = path
        default:
            timer = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { [weak self] _ in
                self?.isCompound = false
                self?.processPath()
            }
        }

    }

    func processPath() {
        let cropped = cropDrawing()
        let scaled = scale(image: cropped)
        let centered = center(image: scaled)
        print(centered.alphaValues!)
        let encoded = encode(image: centered)
        processed.image = encoded
    }

    func cropDrawing() -> UIImage {
        let x = path.bounds.origin.x - lineWidth
        let y = path.bounds.origin.y - lineWidth
        let height = path.bounds.height + 2*lineWidth
        let width = path.bounds.width + 2*lineWidth
        let rect = CGRect(x: x, y: y, width: width, height: height)
        let renderer = UIGraphicsImageRenderer(bounds: rect)
        return renderer.image { _ in
            path.stroke()
        }
    }

    func scale(image: UIImage) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: digitSize)
        return renderer.image { _ in
            image.draw(in: CGRect(origin: .zero, size: digitSize))
        }
    }

    func center(image: UIImage) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: processSize)
        return renderer.image { _ in
            image.draw(at: CGPoint(x: 4, y: 4))
        }
    }

    func encode(image: UIImage) -> UIImage {
        let pixel = image.alphaValues!
        let provider = CGDataProvider(dataInfo: nil, data: pixel, size: 28*28, releaseData: { _ in })!
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let cgImage = CGImage(width: 28, height: 28, bitsPerComponent: 8, bitsPerPixel: 8, bytesPerRow: 28, space: colorSpace, bitmapInfo: .byteOrderMask, provider: provider, decode: nil, shouldInterpolate: false, intent: .defaultIntent)!
        return UIImage(cgImage: cgImage)
    }

}

class GlyphView: UIView {
    var path: UIBezierPath? {
        didSet {
            setNeedsDisplay()
        }
    }

    var lineWidth: CGFloat = 6

    override func draw(_ rect: CGRect) {
        guard let path = path else { return }
        path.lineWidth = lineWidth
        path.lineCapStyle = .round
        path.lineJoinStyle = .round
        path.stroke()
    }
}

extension UIImage {
    var alphaValues: [UInt8]? {
        let dataSize = Int(size.width * size.height)
        var pixelData = [UInt8](repeating: 0, count: dataSize)
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let context = CGContext(data: &pixelData,
                                width: Int(size.width),
                                height: Int(size.height),
                                bitsPerComponent: 8,
                                bytesPerRow: Int(size.width),
                                space: colorSpace,
                                bitmapInfo: CGImageAlphaInfo.alphaOnly.rawValue)
        context?.draw(cgImage!, in: CGRect(origin: .zero, size: size))
        return pixelData
    }
}

let vc = ViewController()
PlaygroundPage.current.liveView = vc
